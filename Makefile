POSTGRES_CONTAINER_NAME=postgres-db
WEB_CONTAINER_NAME=odoo-web
MODULE_NAME=

h: ## help dialog
	@fgrep -h "##" $(MAKEFILE_LIST) | fgrep -v fgrep | sed -e 's/\\$$//' | sed -e 's/##//'

exec_web: ## enter web container terminal
	docker exec -it ${WEB_CONTAINER_NAME} bash

exec_db: ## enter web container terminal
	docker exec -it ${POSTGRES_CONTAINER_NAME} bash

create: ## create custom module for Linux based OS. CMD like "make create name=youre_module_name"
	chmod 777 -R ./custom-addons/;
	docker exec -it ${WEB_CONTAINER_NAME} chown -R ${whoami}:${whoami} /mnt/extra-addons/;
	docker exec -it ${WEB_CONTAINER_NAME} /usr/bin/odoo scaffold ${name} /mnt/extra-addons;
	docker exec -it ${WEB_CONTAINER_NAME} chown -R ${whoami}:${whoami} /mnt/extra-addons/${name}

clean: ## delete all customs addons-path "*/migrations/*"
	sudo rm -f -R ./custom-addons/*
